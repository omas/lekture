/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "customsettingsdialog.h"

// ui
#include "ui_appearance_settings.h"
#include "ui_recording_settings.h"
#include "ui_audio_settings.h"
#include "ui_tablet_settings.h"

// Qt
#include <QWidget>

namespace Lekture
{
  
CustomSettingsDialog::CustomSettingsDialog ( QWidget* parent, const QString& name, KConfigSkeleton* config ) : KConfigDialog ( parent, name, config )
{
    // Construct the dialog subpages
    // First the "Appearance" subpage
    QWidget *appearanceSettingsDlg = new QWidget;
    Ui::appearance_settings ui_appearance_settings;
    ui_appearance_settings.setupUi ( appearanceSettingsDlg );
    
    // Then the "Recording" subpage
    QWidget *recordingSettingsDlg = new QWidget;
    Ui::recording_settings ui_recording_settings;
    ui_recording_settings.setupUi ( recordingSettingsDlg );
    // Manually set some combo initial strings to be able to add proper translation context
    ui_recording_settings.kcfg_Quality->clear();
    ui_recording_settings.kcfg_Quality->addItem(i18nc("Adjective, value for recording quality","Low"));
    ui_recording_settings.kcfg_Quality->addItem(i18nc("Adjective, value for recording quality","Medium"));
    ui_recording_settings.kcfg_Quality->addItem(i18nc("Adjective, value for recording quality","High"));
    ui_recording_settings.kcfg_Quality->addItem(i18nc("Adjective, value for recording quality","Custom (set it below)"));
    ui_recording_settings.kcfg_Channels->clear();
    ui_recording_settings.kcfg_Channels->addItem(i18nc("Refers to the number of audio channels to record","1 (Mono)"));
    ui_recording_settings.kcfg_Channels->addItem(i18nc("Refers to the number of audio channels to record","2 (Stereo)"));
    
    // Then the "Audio" subpage
    QWidget *audioSettingsDlg = new QWidget;
    Ui::audio_settings ui_audio_settings;
    ui_audio_settings.setupUi( audioSettingsDlg );
    audioSrcComboPtr = ui_audio_settings.AudioSource;
    sndSysComboPtr = ui_audio_settings.kcfg_SoundSystem;
    customQualityPtr = ui_recording_settings.kcfg_CustomQuality;
    // Make Audio Source Combo aware of Sound System Combo
    audioSrcComboPtr->setSoundSystemCombo(sndSysComboPtr);
    // Enable/disable quality slider according to chosen option
    connect(ui_recording_settings.kcfg_Quality, SIGNAL(currentIndexChanged(int)), this, SLOT(customQualityActivation(int)));
//     connect(this, SIGNAL(settingsChanged(QString)), audioSrcComboPtr, SLOT(loadSettings()));
    connect(audioSrcComboPtr, SIGNAL(otherThanDefaultSelected(QString)), this, SLOT(settingsChangedSlot()));
    connect(this, SIGNAL(applyClicked()), audioSrcComboPtr, SLOT(applyChanges()));
    connect(this, SIGNAL(okClicked()), audioSrcComboPtr, SLOT(applyChanges()));
    connect(this, SIGNAL(cancelClicked()), audioSrcComboPtr, SLOT(discardChanges()));
    connect(this, SIGNAL(defaultClicked()), audioSrcComboPtr, SLOT(setDefaults()));
    // Finally the "Tablet" subpage
    QWidget *tabletSettingsDlg = new QWidget;
    Ui::tablet_settings ui_tablet_settings;
    ui_tablet_settings.setupUi ( tabletSettingsDlg );
    
    // Add subpages
    addPage ( appearanceSettingsDlg, i18nc ( "Settings tab title", "Appearance" ), "preferences-desktop-display-color" );
    addPage ( recordingSettingsDlg, i18nc ( "Settings tab title", "Recording" ), "media-record" );
    addPage ( audioSettingsDlg, i18nc ( "Settings tab title", "Audio input" ), "audio-headset" );
    addPage ( tabletSettingsDlg, i18nc ( "Settings tab title", "Tablet" ), "input-tablet" );
}


bool CustomSettingsDialog::hasChanged()
{
    return KConfigDialog::hasChanged() || audioSrcComboPtr->hasChanged();
}


bool CustomSettingsDialog::isDefault()
{
    return KConfigDialog::isDefault() && audioSrcComboPtr->isDefault();
}

void CustomSettingsDialog::customQualityActivation (int index)
{
    if (index==3)
        customQualityPtr->setEnabled(true);
    else
        customQualityPtr->setEnabled(false);
}


} // namespace Lekture
