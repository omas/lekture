/*
    Class that provides a scrolled view of the blackboard's surface
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// own
#include "boardview.h"
#include "blackboard.h"

// Qt
#include <QScrollBar>
#include <QDebug>

namespace Lekture
{

BoardView::BoardView ( QWidget* parent, const QSize viewportSize ) : QScrollArea ( parent )
{
    // Desired newSize goes into viewport
    m_viewportSize = viewportSize;
    viewport()->setMinimumSize ( m_viewportSize );
}


QSize BoardView::sizeHint() const
{
    QSize updatedSize = m_viewportSize;
    updatedSize.rwidth() += verticalScrollBar()->width() + 2*frameWidth();
    updatedSize.rheight() += 2*frameWidth();
    return ( updatedSize );
}

QRect BoardView::visibleBoardRect() const
{
    // Top-Left and Bottom-Right points
    QPoint globalOrigin;
    QSize size;
    globalOrigin = mapToGlobal ( QPoint ( viewport()->x() , viewport()->y() ) );
    size = QSize ( viewport()->width() , viewport()->height() );
    QRect myGlobalRect ( globalOrigin , size );
    return myGlobalRect;
}

void BoardView::updateBoardViewSize ( const QSize newSize )
{
    if ( newSize != m_viewportSize ) {
        m_viewportSize = newSize;
        viewport()->setMinimumSize ( newSize );
        adjustSize();
    }
}


}  //Namespace

