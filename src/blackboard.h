/***************************************************************************
 *   Copyright (C) 2012 by Orestes Mas <orestes@tsc.upc.edu>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/


#ifndef BLACKBOARD_H
#define BLACKBOARD_H


// Qt
#include <QWidget>
#include <QStateMachine>

class QStateMachine;
class QImage;
class QPen;
class QColor;
class QSize;

namespace Lekture
{

/**
 * This is the main lekture widget: A blackboard where teacher writes
 *
 * @short Blackboard
 * @author Orestes Mas <orestes@tsc.upc.edu>
 * @version 0.1
 */
class BlackBoard : public QWidget
{
    Q_OBJECT

public:
    /**
     * Default constructor
     */
    explicit BlackBoard ( const QSize initialSize );

    /**
     * Destructor
     */
    virtual ~BlackBoard();

    void clear();
    void updateBoardSize ( const QSize newSize );
    bool loadFile ( const QString &inputFilename );
    bool saveFile ( QString outputFileName );

signals:
    void penColorChanged ( const QColor newColor );
    void boardSizeChanged();
    void chalkSelected();
    void eraserSelected();
    void toChalk();
    void toEraser();

public slots:
    void changeChalkColor ( const QColor color );
    void selectChalk();
    void selectEraser();
    void loadSettings();

protected:
    void mousePressEvent ( QMouseEvent *event );
    void mouseMoveEvent ( QMouseEvent *event );
    void mouseReleaseEvent ( QMouseEvent *event );
    void paintEvent ( QPaintEvent *event );
    void tabletEvent ( QTabletEvent *event );

private slots:
    void enterDrawingState();
    void enterErasingState();

private:
    // Where drawing actually takes place
    QImage *brd;
    // Board size
    QSize boardSize;

    // Pen settings: size and colors
    typedef struct {
        QPen *stylus;
        QCursor *cursor;
    } DrawingTool;

    // We'll use 2 different tools: a pencil and a eraser
    DrawingTool *chalk, *eraser;

    // Drawing goes from previous to current mouse position
    QPoint prevPos, curPos;
    // Whether we're painting or not. We're painting while mouse left button is pressed.
    bool is_painting;

    // State machinery
    enum State {Drawing, Erasing};
    State currentState;
    QStateMachine *m_machine;

    void tabletPaint ( QPainter &painter, QTabletEvent *event );
    void setBackgroundColor ( const QColor newColor );
    void setChalkColor ( const QColor newColor );
    void setChalkWidth ( const double newWidth );
    void setEraserWidth ( const double newWidth );
    void setupDrawingTools();
    void setupStateMachine();
};

} // namespace Lekture

#endif // BLACKBOARD_H
