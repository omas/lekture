/*
    Dummy class to work around a bug in Qt Designer and/or kdelibs
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// own
#include "mycolorcells.h"

MyColorCells::MyColorCells ( QWidget *parent ) : KColorCells ( parent,1,1 )
{
    // We don't want the color clicked to remain selected...
    connect ( this, SIGNAL(colorSelected(int,QColor)), this, SLOT(clearSelection()) );
}
