/*
    Custom class derived from QComboBox, knowing how to fill itself with
    apropiate values, and to save the selected value as String in KConfig
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// own
#include "myaudiocombo.h"
#include "settings.h"

//KDE
#include <KDebug>
#include <KGlobal>
#include <KComponentData>

namespace Lekture
{

/* This class is only instantiated within the config dialog.
 * At this moment one can be sure that application is running
 * and Settings object has been set up
 */

MyAudioCombo::MyAudioCombo(QWidget* parent): QComboBox(parent)
{
    setSizeAdjustPolicy(AdjustToContents);
    config = KSharedConfig::openConfig();
    audioGroup = config->group("Audio input");
    readConfig();   // This will set last and current audio devices to stored values
    // Read current device list and try to select the stored one
    refreshAudioDeviceList ( Settings::soundSystem().toShort() );
    changed = false;
    connect(this,SIGNAL(currentIndexChanged(QString)),this,SLOT(itemChanged(QString)));
}


MyAudioCombo::~MyAudioCombo()
{
}


bool MyAudioCombo::hasChanged()
{
    return (currentALSADevice != lastALSADevice) || (currentPulseDevice != lastPulseDevice);
}


bool MyAudioCombo::isDefault()
{
    // Return false if either current alsa device or current pulse device differ from config
    // and true otherwise
    return (currentALSADevice == lastALSADevice) && (currentPulseDevice == lastPulseDevice);
}


void MyAudioCombo::setRecorder(Recorder* newRecPtr)
{
    recPtr = QPointer<Recorder>(newRecPtr);
}


void MyAudioCombo::setSoundSystemCombo(KComboBox* newSndSysCombo)
{
    sndSysCombo = QPointer<KComboBox>(newSndSysCombo);
    connect (sndSysCombo, SIGNAL (currentIndexChanged(int)), this, SLOT(refreshAudioDeviceList(int)));
}


void MyAudioCombo::refreshAudioDeviceList(int newSndSys)
{
  if (!recPtr.isNull()) {
    // Temporarily disallow calls to itemChanged() while re-populating the list
    disconnect(this,SIGNAL(currentIndexChanged(QString)),this,SLOT(itemChanged(QString)));
    clear();
    switch ( recPtr->translateSndSys(newSndSys) ) {
      case Recorder::ALSA:
        insertItems(0,recPtr->getAudioDeviceList(Recorder::ALSA));
        setCurrentIndex(findText(currentALSADevice,Qt::MatchExactly));
        break;
      case Recorder::PulseAudio:
        insertItems(0,recPtr->getAudioDeviceList(Recorder::PulseAudio));
        setCurrentIndex(findText(currentPulseDevice,Qt::MatchExactly));
        break;
    }
    // Job done: reconnect to handle correctly further user manipulation
    connect(this,SIGNAL(currentIndexChanged(QString)),this,SLOT(itemChanged(QString)));
  }
}


void MyAudioCombo::applyChanges()
{
    writeConfig();
}


void MyAudioCombo::discardChanges()
{
    readConfig();
}


void MyAudioCombo::setDefaults()
{
    currentALSADevice = Recorder::disabledAudioString.toString();
    currentPulseDevice = Recorder::disabledAudioString.toString();
    refreshAudioDeviceList(Settings::soundSystem().toShort());
}


bool MyAudioCombo::event(QEvent* event)
{
    bool result = QComboBox::event(event);
    // We do need to catch the "polish" event to do some delayed initialization outside the constructor
    if ((event->type() == QEvent::PolishRequest) && (!sndSysCombo.isNull())) {
        event->accept();
        refreshAudioDeviceList ( sndSysCombo->currentIndex() );
	return true;
    }
    return result;
}


void MyAudioCombo::itemChanged(QString newItem)
{
    // We need a valid pointer to Sound System Combo and we don't have to be clearing the list
    if ( (!sndSysCombo.isNull()) && (!newItem.isEmpty()) ) {
        // Sets current item to new item
        switch ( recPtr->translateSndSys(sndSysCombo->currentIndex()) ) {
          case Recorder::ALSA:
              currentALSADevice = newItem;
              break;
          case Recorder::PulseAudio:
              currentPulseDevice = newItem;
              break;
        }
        // Test if selected items differ from ones stored into config
        changed = (currentALSADevice != lastALSADevice) || (currentPulseDevice != lastPulseDevice);
	if (changed) emit otherThanDefaultSelected(currentText());
    }
}


void MyAudioCombo::readConfig()
{
    // Read last values from config, or set defaults if empty
    lastALSADevice = currentALSADevice = audioGroup.readEntry("Last_ALSA_device",Recorder::disabledAudioString.toString());
    lastPulseDevice = currentPulseDevice = audioGroup.readEntry("Last_Pulse_device",Recorder::disabledAudioString.toString());
}


void MyAudioCombo::writeConfig()
{
    // If current device differs from device stored in config file
    // then replace stored device with current one
    switch ( recPtr->translateSndSys(sndSysCombo->currentIndex()) ) {
      case Recorder::ALSA:
          lastALSADevice = currentALSADevice;
          audioGroup.writeEntry("Last_ALSA_device",lastALSADevice);
          break;
      case Recorder::PulseAudio:
          lastPulseDevice = currentPulseDevice;
          audioGroup.writeEntry("Last_Pulse_device",lastPulseDevice);
          break;
    }
    audioGroup.sync();
    changed = false;
}


} // namespace Lekture
