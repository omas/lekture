/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// own
#include "chalksdockwidget.h"
#include "blackboard.h"

// ui
#include "ui_chalks_dockwidget.h"

namespace Lekture
{

ChalksDockWidget::ChalksDockWidget ( QWidget* parent, Qt::WindowFlags flags )
    : QDockWidget ( parent, flags )
{
    setupUi ( this );

    int i;
    QColor col;
    // "BW" cells have different gray levels
    for ( i=0; i<5; i++ ) {
        col.setHsvF ( 0,0,i/4.0 );
        bw->setColor ( i,col );
    }
    // "colors" cells have evenly distributed colors in HSV space, at 4 different saturation levels
    for ( i=0; i<18; i++ ) {      //krazy:exclude=c++-postfixop
        col.setHsvF ( ( 20.0*i ) /360,1,1 );
        colors->setColor ( 4*i,col );
        col.setHsvF ( ( 20.0*i ) /360,0.75,1 );
        colors->setColor ( 4*i+1,col );
        col.setHsvF ( ( 20.0*i ) /360,0.5,1 );
        colors->setColor ( 4*i+2,col );
        col.setHsvF ( ( 20.0*i ) /360,0.25,1 );
        colors->setColor ( 4*i+3,col );
    }

    // Make KColorCells signals perform some actions
    connect ( bw, SIGNAL(colorSelected(int,QColor)),this, SLOT(colorClicked(int,QColor)) );
    connect ( colors, SIGNAL(colorSelected(int,QColor)),this, SLOT(colorClicked(int,QColor)) );
    connect ( buttonGroup, SIGNAL(buttonClicked(int)), this, SLOT(toolToggled(int)) );
}


QSize ChalksDockWidget::sizeHint() const
{
    return QSize ( 90,360 );
}


void ChalksDockWidget::changeCurrentColor ( const QColor newColor )
{
    current->setColor ( 0, newColor );
}

void ChalksDockWidget::selectEraser()
{
    eraserButton->setChecked ( true );
}

void ChalksDockWidget::selectChalk()
{
    chalkButton->setChecked ( true );
}

void ChalksDockWidget::colorClicked ( int index, const QColor newColor )
{
    Q_UNUSED ( index )
    emit newColorRequested ( newColor );
}

void ChalksDockWidget::toolToggled ( int buttonNumber )
{
    Q_UNUSED ( buttonNumber )
    if ( chalkButton->isChecked() ) {
        emit chalkSelected();
    } else {
        emit eraserSelected();
    }
}

} // namespace Lekture

