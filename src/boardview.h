/*
    Class that provides a scrolled view of the blackboard's surface
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef BOARDVIEW_H
#define BOARDVIEW_H

#include <QScrollArea>

namespace Lekture
{

class BoardView : public QScrollArea
{

public:
    explicit BoardView ( QWidget* parent = 0 , const QSize viewportSize = QSize ( 100,100 ) );
    virtual QSize sizeHint() const;
    QRect visibleBoardRect() const;
    void updateBoardViewSize ( const QSize newSize );

private:
    QSize m_viewportSize;
};

}  //Namespace

#endif // BOARDVIEW_H
