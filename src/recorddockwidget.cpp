﻿/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


// own
#include "recorddockwidget.h"
#include "settings.h"
#include "mainwindow.h"

// Qt
#include <QDesktopWidget>
#include <QSignalTransition>

//KDE
#include <KMessageBox>
#include <KApplication>

// ui
#include "ui_record_dockwidget.h"

namespace Lekture
{

RecordDockWidget::RecordDockWidget (QWidget* parent, Qt::WindowFlags flags) : QDockWidget (parent, flags)
{
    setupUi (this);
    m_recorder = new Recorder (this);
    setupStateMachine();
}


RecordDockWidget::~RecordDockWidget()
{
    delete m_machine;
    delete m_recorder;
}


Recorder* RecordDockWidget::recorder()
{
    return m_recorder;
}


void RecordDockWidget::loadSettings()
{
}


void RecordDockWidget::idleStateEntered()
{
    /* Tasks to do:
     * 1) Set proper button and led states
     * 2) Notify Main Window we've entered recording state
     */
    currentState = Idle;
    is_valid_recordingrect = false;
    recLed->off();
    recButton->setText (i18nc ("Button text","REC"));
    recButton->setIcon (KIcon ("media-record", KIconLoader::global()));
    recButton->setChecked (false);
    recButton->setCheckable (false);
    m_recorder->stop();
//     static_cast<MainWindow*>(parent())->enterIdleState();
}


void RecordDockWidget::recordingStateEntered()
{
    /* Tasks to do:
     * 1) Set proper button and led states
     * 2) Notify Main Window we've entered recording state
     * 3) (TODO) Set up audio volume monitoring to display vol level in volume widget ??
     */
    currentState = Recording;
    recLed->on();
    recButton->setText (i18nc ("Button text","STOP"));
    recButton->setIcon (KIcon ("media-playback-stop", KIconLoader::global()));
    recButton->setCheckable (true);
    recButton->setChecked (true);
}


// Boolean functions used to validate state transitions
bool RecordDockWidget::canRecord()
{
    /* Tasks to do:
     * 1) Resize main window?
     * 2) Check recording area validity (all area visible)
     * 3) Check recording path validity (existence and permissions)
     * 4) Calculate next filename
     * 5) Try to launch recording
     */

    // Path checking
    // Get saving dir from configuration, and "massage" it a little
    KUrl url = Settings::savingPath();
    url.cleanPath();
    url.adjustPath (KUrl::AddTrailingSlash);
    QDir recordingDir = QDir (url.directory (KUrl::ObeyTrailingSlash | KUrl::AppendTrailingSlash));
    if (!checkDirectory (recordingDir)) {
        return false;
    }

    //FIXME: Resize window to proper size
    
    // Recording area checking
    QRect recordingRect = static_cast<MainWindow*> (parent())->getRecordingRect();
    if (!checkRecordingRect (recordingRect)) {
        return false;
    }

    // Next filename calculation
    QString filename = nextFilename (recordingDir);
    if (!filename.isEmpty())
        url.setPath(filename);
    else return false;

    // Try to start recording. On success, a signal emitted by recorder will trigger the transition to the
    return m_recorder->start (recordingRect, url, Recorder::formatMap[Settings::format()]);
}


void RecordDockWidget::setupStateMachine()
{
    // Define mechine and states
    m_machine = new QStateMachine (this);
    QState* idle = new QState();
    QState* recording = new QState();

    // Define conditional transitions:
    Lekture::ConditionalTransition* idleToRecording;
    idleToRecording = new Lekture::ConditionalTransition (this, &RecordDockWidget::canRecord);
    idleToRecording->setSenderObject (recButton);
    idleToRecording->setSignal (SIGNAL (clicked()));
    idleToRecording->setTargetState (recording);

    idle->addTransition (idleToRecording);
    recording->addTransition (recButton, SIGNAL (clicked()), idle);
    recording->addTransition (m_recorder, SIGNAL (stopped()), idle);

    // Do stuff on state switching
    connect (idle, SIGNAL (entered()), this, SLOT (idleStateEntered()));
    connect (recording, SIGNAL (entered()), this, SLOT (recordingStateEntered()));

    // Build machine and start it
    m_machine->addState (idle);
    m_machine->addState (recording);
    m_machine->setInitialState (idle);
    m_machine->start();
}


/*
 * Checking functions
 */

bool RecordDockWidget::checkDirectory (QDir desiredDir)
{
    QString message;
    // Test if the required dir exists
    if (!desiredDir.exists()) {
        message = i18n("The configured video saving directory does not exist:\n\n%1.\n\n", desiredDir.path());
        message += i18n("Press the button below to create this directory. Otherwise you can cancel and change it in the settings dialog.");
        KGuiItem createButton = KGuiItem (KStandardGuiItem::cont());
        createButton.setText (i18nc ("Button text", "Create"));
        int result = KMessageBox::warningContinueCancel ( (QWidget*) parent(), message, i18nc ("MessageBox title", "Warning"), createButton);
        if (result == KMessageBox::Continue) {
            return (desiredDir.mkpath (desiredDir.absolutePath()));
        } else {
            return false;
        }
    };

    // Test if we have writing permissions in this directory
    QStringList filename (".");
    QFileInfoList info = desiredDir.entryInfoList (filename);
    if (info.count() != 1) {
        qDebug() << "Internal error: getting current directory returned more or less than one entry.";
        return false;
    }
    if (!info.at (0).isWritable()) {
        message = i18n ("It seems you don't have writing permissions for the folder where videos are to be saved.\n\n\
    Please change folder in your settings or change its permissions.");
        KMessageBox::sorry ( (QWidget*) parent(), message, i18nc ("MessageBox title", "Permission error"));
        return false;
    }
    return true;
}


bool RecordDockWidget::checkRecordingRect (QRect desiredRect)
{
    QDesktopWidget* desktop = QApplication::desktop();
    QRegion availableArea = QRegion();
    for (int i = 0; i < desktop->screenCount(); i++) {
        qDebug() << "Desktop " << i << ":" << desktop->availableGeometry (i);
        availableArea += desktop->availableGeometry (i);
    }
    qDebug() << "Available area:" << availableArea;
    qDebug() << "Desired rect:" << desiredRect;
    /* It seems that QRegion::contains() doesn't work well
     * Possibly a bug that should be reported
     */
    if (!availableArea.contains (desiredRect)) {
        QString message = i18n ("Cannot start recording because part of the recording area is offscreen.\n\n\
    Please move Lekture window so that all recording area is visible.");
        KMessageBox::sorry ( (QWidget*) parent(), message, i18nc ("MessageBox title", "Outside limits"));
        return false;
    }
    return true;
}


QString RecordDockWidget::nextFilename (QDir savingDir)
{
    QString prefix = Settings::filePrefix();
    QString suffix;
    QString newName;

    // Set suffix according to video container
    switch (m_recorder->formatMap[Settings::format()]) {
    case Recorder::WebM:
        suffix = ".webm";
        break;
    case Recorder::Ogg:
        suffix = ".ogx";
        break;
    }

    // Get a list of files begginning with our prefix
    QStringList filters = QStringList (prefix + '*');
    savingDir.setFilter (QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot | QDir::CaseSensitive);
    savingDir.setSorting (QDir::Name);
    savingDir.setNameFilters (filters);
    QFileInfoList list = savingDir.entryInfoList();

    // Calculate new name according to desired pattern
    if (Settings::dateTime()) {
        QDateTime now = QDateTime::currentDateTime();
        now.setTimeSpec (Qt::OffsetFromUTC);
        newName = savingDir.canonicalPath() + QDir::separator() + prefix + now.toString (Qt::ISODate) + suffix;
        if (QFile::exists (newName)) {
            //Messagebox
            QString message = i18n ("Could not find an unused sequential filename according to specified pattern.\n\n"
                 "Please try to delete some files from the destination folder, or select another pattern/folder.");
            KMessageBox::error ( (QWidget*) parent(), message, i18nc ("MessageBox title", "Internal error"));
            return "";
        } else return newName;
    } else {
        // Set regular expression to detect numbers between prefix and suffix
        //FIXME: Hard-coded an arbitrary max number of 10 digits. Perhaps could be user-selectable
        QRegExp numSearch ("^" + prefix + "[0-9]{1,10}" + "\\" + suffix);
        QString result;
        long int max = 0, num = 0;
        // Explore file list and extract filename numbers
        for (int i = 0; i < list.count(); i++) {
            if (numSearch.exactMatch (list.at(i).fileName())) {
                result = numSearch.capturedTexts().takeFirst();
                result.chop (suffix.length());
                result.remove (0, prefix.length());
                num = result.toLong();
                max = (max > num) ? max : num;
            }
        }
        // Max number found. Let's increment it by one
        max++;
        newName = savingDir.canonicalPath() + QDir::separator() + prefix + QString::number (max) + suffix;
        if (QFile::exists (newName)) {
            //Messagebox
            QString message = i18n ("Could not find an unused sequential filename according to specified pattern.\n\n"
                 "Please try to delete some files from the destination folder, or select another pattern/folder.");
            KMessageBox::error ( (QWidget*) parent(), message, i18nc ("MessageBox title", "Internal error"));
            return "";
        } else return newName;
    }
}



/*
 * HELPER CLASS ConditionalTransition
 */
ConditionalTransition::ConditionalTransition (RecordDockWidget* r, condFun f) : QSignalTransition()
{
    widgetPtr = r;
    testFunction = f;
}


bool ConditionalTransition::eventTest (QEvent* event)
{
    if (!QSignalTransition::eventTest (event))
        return false;
    if (testFunction != NULL) {
        return (widgetPtr->*testFunction) ();
    } else {
        return false;
    }
}


} // namespace Lekture

