/*
 *    <one line to give the program's name and a brief idea of what it does.>
 *    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//own
#include "recorder.h"
#include <settings.h>

//Qt and Xlib-related
#include <QDebug>
// #include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <QX11Info>
#include <fixx11h.h>

// QtGstreamer
#include <QGlib/Error>
#include <QGlib/Connect>
#include <QGst/Init>
#include <QGst/ElementFactory>
#include <QGst/PropertyProbe>
#include <QGst/Pipeline>
#include <QGst/Pad>
#include <QGst/Event>
#include <QGst/Message>
#include <QGst/Bus>
#include <QGst/Clock>

//KDE
#include <KConfig>
#include <KMessageBox>

namespace Lekture
{

static QMap<QString, int> fillVresMap()
{
    QMap<QString, int> vrm;
    vrm.insert ("0", 360);
    vrm.insert ("1", 480);
    vrm.insert ("2", 720);
    vrm.insert ("3", 1080);
    return vrm;
}

static QMap<QString, QString> fillAspectMap()
{
    QMap<QString, QString> am;
    am.insert ("0", "4:3");
    am.insert ("1", "16:9");
    return am;
}

static QMap<QString, Recorder::RecordingFormat> fillFormatMap()
{
    QMap<QString, Recorder::RecordingFormat> fm;
    // Initialise mappings
    fm.insert ("0", Recorder::WebM);
    fm.insert ("1", Recorder::Ogg);
    return fm;
}

static QMap<QString, int> fillAudioQualityMap()
{
    QMap<QString, int> aqm;
    // Initialise mappings
    aqm.insert ("0", 20);
    aqm.insert ("1", 50);
    aqm.insert ("2", 80);
    return aqm;
}

QMap<QString, int> Recorder::vresMap = fillVresMap();
QMap<QString, QString> Recorder::aspectMap = fillAspectMap();
QMap<QString, Recorder::RecordingFormat> Recorder::formatMap = fillFormatMap();
QMap<QString,int> Recorder::audioQualityMap = fillAudioQualityMap();

KLocalizedString Recorder::disabledAudioString = ki18nc ("Audio source (settings combo option)", "No Audio");

Recorder::Recorder (QObject* parent) : QObject (parent)
{
    m_AudioDeviceList.prettyNames = QStringList();
    m_AudioDeviceList.deviceNames = QStringList();
    //TODO
    /* Steps to do
     * 1) Find the available audio sources (follow the QtGstreamer example) -> DONE
     * 2) Get the current audio source selected in Settings, and determine if it's a valid one
     * 3) If so, select it
     * 4) If not, select "Auto" in settings
     */
    // Fill the audio device list
    QGst::init();
    //FIXME: Catch possible errors
    m_pipeline = QGst::Pipeline::create();
    findAudioDevices ( (soundSystem) Settings::soundSystem().toInt());
    // Job done: destroy the pointer and clear reference
    m_pipeline->setState (QGst::StateNull);
    m_pipeline.clear();
}



QSize Recorder::getStdVideoSize()
{
    QSize standardVideoSize;
    QString res, ar;
    res = Settings::resolution();
    ar = Settings::aspectRatio();
    if (aspectMap[ar] == "4:3") {
        switch (vresMap[res]) {
        case 360:
            standardVideoSize = QSize (480, 360);
            break;
        case 480:
            standardVideoSize = QSize (640, 480);
            break;
        case 720:
            standardVideoSize = QSize (960, 720);
            break;
        case 1080:
            standardVideoSize = QSize (1440, 1080);
            break;
        }
    } else if (aspectMap[ar] == "16:9") {
        switch (vresMap[res]) {
        case 360:
            standardVideoSize = QSize (640, 360);
            break;
        case 480:
            standardVideoSize = QSize (854, 480);
            break;
        case 720:
            standardVideoSize = QSize (1280, 720);
            break;
        case 1080:
            standardVideoSize = QSize (1920, 1080);
            break;
        }
    }
    return (standardVideoSize);
}

QStringList Recorder::getAudioDeviceList (Recorder::soundSystem sndSys)
{
    findAudioDevices (sndSys);
    return m_AudioDeviceList.prettyNames;
}


Recorder::soundSystem Recorder::translateSndSys (int ss_int)
{
    switch (ss_int) {
    case 0:
        return Recorder::ALSA;
    case 1:
        return Recorder::PulseAudio;
    default:
        abort();
    }
}


bool Recorder::start (QRect rect, KUrl url, RecordingFormat format)
{
    // Code inspired from QtGStreamer "recorder" example, by Collabora Ltd.

    /***********************************************************************/
    /********************** AUDIO SOURCE BIN CREATION **********************/
    /***********************************************************************/
    QGst::BinPtr audioBin;

    QString audioBinDesc, audioSrc, audioEnc;
    // Parametrize audio source type according to config settings
    switch (translateSndSys (Settings::soundSystem().toInt())) {
    case ALSA:
        audioSrc = "alsasrc name=audiosrc";
        break;
    case PulseAudio:
        audioSrc = "pulsesrc name=audiosrc";
        break;
    }
    // Parametrize audio encoder: We use vorbis audio encoder for Ogg and WebM containers
    // Other formats to come
    switch (format) {
    case Ogg:
    case WebM:
        // "3" is custom quality
        if (Settings::quality() == "3") 
            audioEnc = QString("vorbisenc name=audioenc quality=%1").arg((double)(Settings::customQuality()/100), 0, 'f', 2);
        else
            audioEnc = QString("vorbisenc name=audioenc quality=%1").arg((double)(audioQualityMap[Settings::quality()])/100, 0, 'f', 2);
        break;
    }

    // Try to create audio bin and, if success, set it to ready state
    audioBinDesc +=
        audioSrc +
        QString (" ! audio/x-raw-int,rate=%1,channels=%2,depth=16").arg (Settings::samplingRate(), 0, 10).arg (Settings::channels().toInt() + 1, 0, 10) +
        " ! audioconvert ! audiorate"
        " ! " + audioEnc +
        " ! queue";

    try {
        audioBin = QGst::Bin::fromDescription (audioBinDesc);
    } catch (const QGlib::Error& error) {
        qCritical() << "Failed to create audio source bin:" << error;
        return false;
    }
    audioBin->setState (QGst::StateNull);

    // Reload the audio device list. We have to test if desired audio device is still present (e.g. an
    // USB soundcard can be disconnected at any time)
    soundSystem currentSoundSystem = translateSndSys (Settings::soundSystem().toInt());
    findAudioDevices (currentSoundSystem);

    //Get the desired audio card input to record from
    KSharedConfigPtr config = KSharedConfig::openConfig();
    KConfigGroup audioGroup = config->group ("Audio input");
    QString currentAudioDevice;
    switch (currentSoundSystem) {
    case ALSA:
        currentAudioDevice = audioGroup.readEntry ("Last_ALSA_device", Recorder::disabledAudioString.toString());
        break;
    case PulseAudio:
        currentAudioDevice = audioGroup.readEntry ("Last_Pulse_device", Recorder::disabledAudioString.toString());
        break;
    }

    // Here we must translate the "pretty audio device name" into a machine-meaningful device name
    int deviceIndex = m_AudioDeviceList.prettyNames.indexOf (currentAudioDevice);
    if (deviceIndex == -1) {
        // FIXME: Return some info about error, so that parent object can display a messagebox
        return false;
    }
    currentAudioDevice = m_AudioDeviceList.deviceNames.at (deviceIndex);

    // Set the desired audio source into the element's "device" property
    if (!currentAudioDevice.isEmpty() && currentAudioDevice != Recorder::disabledAudioString.toString()) {
        audioBin->getElementByName ("audiosrc")->setProperty ("device", currentAudioDevice);
    }

    /***********************************************************************/
    /********************** VIDEO SOURCE BIN CREATION **********************/
    /***********************************************************************/
    QGst::BinPtr videoBin;
    QString videoBinDesc, videoEnc;
    
    // Bitrate calculation, according to "Kush gauge" (assuming low movement amount)
    //TODO: For now bitrate is fixed (somewhat around a value yelding standard quality)
    //TODO: Let user choose between low, standard, high and custom (with slider) quality.
    double bitrate;
    bitrate = rect.width()*rect.height()*Settings::fps()*0.07;
    // Parametrize video encoder: We use Theora for Ogg, and VP8 for WebM containers
    // Other formats to come
    switch (format) {
    case Ogg:
        // Theora encoder bitrate range is 0-16777215 Kbps
        bitrate=24000000;
        videoEnc = QString("theoraenc bitrate=%1").arg((int)round(bitrate/1000),0,10);
        videoEnc = QString("theoraenc quality=32");
        break;
    case WebM:
        // VP8 encoder bitrate range is 0-1000000 Kbps, but must be expressed in bps
        bitrate = 24000000;
        videoEnc = QString("vp8enc bitrate=%1").arg((double)round(bitrate),0,'f',0);
        videoEnc = QString("vp8enc quality=7 speed=2");
        break;
    }

    // Try to create video bin and, if success, set it to ready state
    videoBinDesc +=
        "ximagesrc name=videosrc"
        " ! cogcolorspace"
        " ! videorate" +
        QString (" ! video/x-raw-yuv,framerate=%1/1").arg (Settings::fps(), 0, 10) +
        " ! " + videoEnc +
        " ! queue";

    try {
        videoBin = QGst::Bin::fromDescription (videoBinDesc);
    } catch (const QGlib::Error& error) {
        qCritical() << "Failed to create video source bin:" << error;
        return false;
    }
    videoBin->setState (QGst::StateNull);

    //set the source's properties
    QGst::ElementPtr videoSrc = videoBin->getElementByName ("videosrc");
    videoSrc->setProperty ("display-name", QString (DisplayString (QX11Info::display())));
    videoSrc->setProperty ("screen-num", QX11Info::appScreen());
    videoSrc->setProperty ("startx", rect.left());
    videoSrc->setProperty ("endx", rect.right());
    videoSrc->setProperty ("starty", rect.top());
    videoSrc->setProperty ("endy", rect.bottom());
    videoSrc->setProperty ("do-timestamp", true);
    videoSrc->setProperty ("blocksize", 65536);
    videoSrc->setProperty ("use-damage", false);    // <<< MANDATORY !! (failing to set it leads to "stutter" video)

    /***********************************************************************/
    /******************* THEN THE MUXER AND OTHER ELEMENTS *****************/
    /***********************************************************************/
    // Create the muxer element (accordingly to the file format we want)
    QGst::ElementPtr mux;
    const char* audiopadname, *videopadname;
    switch (format) {
    case Ogg:
        mux = QGst::ElementFactory::make ("oggmux");
        mux->setState (QGst::StateNull);
        audiopadname = "sink_%d";
        videopadname = "sink_%d";
        break;
    case WebM:
        mux = QGst::ElementFactory::make ("webmmux");
        mux->setState (QGst::StateNull);
        mux->setProperty ("writing-app", "lekture");
        audiopadname = "audio_%d";
        videopadname = "video_%d";
        break;
    }

    // And finally the sink element, which is a file (we tune its parameters later)
    QGst::ElementPtr sink = QGst::ElementFactory::make ("filesink");

    if (!audioBin || !videoBin || !mux || !sink) {
        //FIXME: convert to KMessageBox
        //         QMessageBox::critical(this, tr("Error"), tr("One or more elements could not be created. "
        //                               "Verify that you have all the necessary element plugins installed."));
        return false;
    }

    sink->setProperty ("location", url.toLocalFile());

    // Final step: try to build and start the pipeline
    try {

        m_pipeline = QGst::Pipeline::create();
        m_pipeline->add (audioBin, videoBin, mux, sink);
        
        //link elements
        QGst::PadPtr audioPad = mux->getRequestPad (audiopadname);
        audioBin->getStaticPad ("src")->link (audioPad);

        QGst::PadPtr videoPad = mux->getRequestPad (videopadname);
        videoBin->getStaticPad ("src")->link (videoPad);

        mux->link (sink);

        //connect the bus
        m_pipeline->bus()->addSignalWatch();
        QGlib::connect (m_pipeline->bus(), "message", this, &Recorder::onBusMessage);

        // Use system clock, otherwise sometimes pipeline doesn't start
        m_pipeline->useClock(QGst::Clock::systemClock());
        
        //go!
        QGst::StateChangeReturn retVal =  m_pipeline->setState (QGst::StatePlaying);
        bool success = false;
        switch (retVal) {
        case QGst::StateChangeFailure:
            break;
        case QGst::StateChangeSuccess:
            success = true;
            break;
        case QGst::StateChangeAsync:
        case QGst::StateChangeNoPreroll:
            //FIXME: What's to be done in these two cases?
//             retVal = m_pipeline->getState (NULL, NULL, QGst::ClockTime::fromTime (QTime (0, 0, 2, 0))); //2-seconds timeout
            success = true;
            break;
        }
        if (success) {
            emit started();
        } else {
            destroyPipeline();
        }
        return success;
    } catch (const QGlib::Error& error) {
        destroyPipeline();
        return false;
    }
}


void Recorder::stop()
{
    if (!m_pipeline.isNull()) {
        //stop recording by sending EOS event that flushes buffers
        m_pipeline->sendEvent (QGst::EosEvent::create());
    }
}


void Recorder::destroyPipeline()
{
    if (!m_pipeline.isNull()) {
        m_pipeline->setState (QGst::StateNull);
        //clear the pointer, destroying the pipeline as its reference count drops to zero.
        m_pipeline.clear();
        //emit signal to notify others
        emit stopped();
    }
}


void Recorder::onBusMessage (const QGst::MessagePtr& message)
{
    switch (message->type()) {
    case QGst::MessageEos:
        //got end-of-stream - stop the pipeline
        destroyPipeline();
        break;
    case QGst::MessageError:
        //check if the pipeline exists before destroying it,
        //as we might get multiple error messages
        if (m_pipeline) {
            destroyPipeline();
        }
        qDebug() << "Pipeline error";
        //FIXME: convert to KMessageBox
        //         QMessageBox::critical(this, tr("Pipeline Error"),
        //                               message.staticCast<QGst::ErrorMessage>()->error().message());
        break;
    default:
        break;
    }
}



void Recorder::findAudioDevices (soundSystem sndSys)
{
    QGst::PropertyProbePtr audioProbe;
    QGst::ElementPtr src;
    m_AudioDeviceList.prettyNames.clear();
    m_AudioDeviceList.deviceNames.clear();
    switch (sndSys) {
    case ALSA:
        src = QGst::ElementFactory::make ("alsasrc");
        if (!src) {
            KMessageBox::error (NULL, i18n ("Failed to create element \"alsasrc\". Make sure you have gstreamer-plugins-base installed"),
                                      i18nc ("MessageBox title", "Error")
            );
        }
        break;
    case PulseAudio:
        src = QGst::ElementFactory::make ("pulsesrc");
        if (!src) {
            KMessageBox::error (NULL, i18n ("Failed to create element \"pulsesrc\". Make sure you have gstreamer-plugins-good installed"),
                                         i18nc ("MessageBox title", "Error")
            );
        }
        break;
    }

    audioProbe = src.dynamicCast<QGst::PropertyProbe>();

    //Most sources and sinks have a "device" property which supports probe
    //and probing it returns all the available devices on the system.
    //Here we try to make use of that to list the system's devices
    //and if it fails, we just leave the source to use its default device.
    if (audioProbe && audioProbe->propertySupportsProbe ("device")) {
        //get a list of devices that the element supports
        QList<QGlib::Value> devices = audioProbe->probeAndGetValues ("device");

        m_AudioDeviceList.prettyNames << Recorder::disabledAudioString.toString();
        m_AudioDeviceList.deviceNames << Recorder::disabledAudioString.toString();
        Q_FOREACH (const QGlib::Value & device, devices) {
            //set the element's device to the current device and retrieve its
            //human-readable name through the "device-name" property
            audioProbe->setProperty ("device", device);
            QString deviceName = audioProbe->property ("device-name").toString();
            //add the device on the combobox
            // ALSA and PulseAudio are treated differently due to gstreamer inconsistencies
            QString stringToInsert;
            switch (sndSys) {
            case ALSA:
                stringToInsert = QString ("%1 (%2)").arg (deviceName, device.toString());
                m_AudioDeviceList.prettyNames << stringToInsert;
                m_AudioDeviceList.deviceNames << device.toString();
                break;
            case PulseAudio:
                stringToInsert = QString ("%1").arg (device.toString());
                m_AudioDeviceList.prettyNames << stringToInsert;
                m_AudioDeviceList.deviceNames << stringToInsert;
                break;
            }
        }
        if (sndSys == ALSA) {
            m_AudioDeviceList.prettyNames << i18nc("Audio source (settings combo option)", "PulseAudio's ALSA emulation");
            m_AudioDeviceList.deviceNames << "default";
        }
    } else {
        m_AudioDeviceList.prettyNames << i18nc ("Audio source (settings combo option)", "Default device");
        m_AudioDeviceList.deviceNames << "default";
    }
    src.clear();
    audioProbe.clear();
}

} // namespace Lekture
