/***************************************************************************
 *   Copyright (C) 2012 by Orestes Mas <orestes@tsc.upc.edu>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H


// own
#include "blackboard.h"
#include "boardview.h"
#include "chalksdockwidget.h"
#include "recorddockwidget.h"
#include "customsettingsdialog.h"

// Qt
#include <QStateMachine>

//KDE
#include <KXmlGuiWindow>

class KToggleAction;


namespace Lekture
{

/**
 * This class serves as the main window for lekture.  It handles the
 * menus, toolbars, status bars and also central
 *
 * @short Main window class
 * @author Orestes Mas <orestes@tsc.upc.edu>
 * @version 0.1
 */
class MainWindow : public KXmlGuiWindow
{
    Q_OBJECT

public:
    explicit MainWindow ( QWidget *parent = 0 );
    virtual ~MainWindow();

private:
    BlackBoard *m_board;
    BoardView *m_boardView;
    RecordDockWidget *rdw;
    ChalksDockWidget *cdw;
    CustomSettingsDialog *settingsDialog;

    KToggleAction *m_toolbarAction;
    KToggleAction *m_statusbarAction;

    QString fileName;

    void setupActions();
    QSize realBoardSize();

protected:
    void enterIdleState();
    void enterRecordingState();
    QRect getRecordingRect();
    friend class RecordDockWidget;  //This class can access protected methods directly

private slots:
    void boardSizeChanged();
    // Actions
    void clear();
    void loadSettings();
    void optionsPreferences();
    void loadFile();
    void saveFile();
    void saveFileAs();
    void saveFileAs ( const QString &outputFileName );
};

} //namespace Lekture

#endif // _MAINWINDOW_H_
