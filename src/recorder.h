/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef RECORDER_H
#define RECORDER_H

// Qt
#include <QDir>
#include <QStringList>
#include <QRect>

//QtGStreamer
#include <QGst/PropertyProbe>
#include <QGst/Pipeline>

//KDE
#include <KUrl>
#include <KLocalizedString>

namespace Lekture
{

struct AudioDeviceList {
    QStringList prettyNames;
    QStringList deviceNames;
};

class Recorder : public QObject
{
    Q_OBJECT
    Q_ENUMS(RecordingFormat)
    
public:
    enum deviceFormat {Description, Device};
    enum soundSystem {ALSA, PulseAudio};
    enum RecordingFormat {Ogg,WebM};
    static KLocalizedString disabledAudioString;
    static QMap<QString,int> vresMap;
    static QMap<QString,QString> aspectMap;
    static QMap<QString,RecordingFormat> formatMap;
    static QMap<QString,int> audioQualityMap;

    explicit Recorder(QObject* parent);

    QSize getStdVideoSize();
    QStringList getAudioDeviceList(soundSystem sndSys);
    soundSystem translateSndSys(int ss_int);

    bool start(QRect rect, KUrl url, Lekture::Recorder::RecordingFormat format);
    void stop();
    
signals:
    void started();
    void stopped();
    
private:
    AudioDeviceList m_AudioDeviceList;

    QRect m_recordingRect;

    QGst::PipelinePtr m_pipeline;
    void destroyPipeline();
    void findAudioDevices(soundSystem sndSys);
    void onBusMessage(const QGst::MessagePtr &message);
};

} //namespace Lekture
#endif // RECORDER_H
