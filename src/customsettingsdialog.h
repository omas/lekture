/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CUSTOMSETTINGSDIALOG_H
#define CUSTOMSETTINGSDIALOG_H

// KDE
#include <KConfigDialog>
#include <KComboBox>
#include <KIntNumInput>

//own
#include "myaudiocombo.h"

namespace Lekture
{

class CustomSettingsDialog : public KConfigDialog
{
    Q_OBJECT

public:
    CustomSettingsDialog ( QWidget* parent, const QString& name, KConfigSkeleton* config );
    KComboBox *sndSysComboPtr;
    KIntNumInput *customQualityPtr;
    MyAudioCombo *audioSrcComboPtr;
    
protected:
    virtual bool hasChanged();
    virtual bool isDefault();

private slots:
    void customQualityActivation(int index);
    
};


} // namespace Lekture

#endif // CUSTOMSETTINGSDIALOG_H
