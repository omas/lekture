/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef RECORDDOCKWIDGET_H
#define RECORDDOCKWIDGET_H

// own
#include "ui_record_dockwidget.h"
#include "recorder.h"

//Qt
#include <QStateMachine>
#include <QSignalTransition>

namespace Lekture
{
    
class RecordDockWidget : public QDockWidget, public Ui::recordDockWidget
{
    Q_OBJECT

public:
    explicit RecordDockWidget ( QWidget* parent = 0, Qt::WindowFlags flags = 0 );
    virtual ~RecordDockWidget();
    Recorder* recorder();

public slots:
    void loadSettings();

private slots:
    // Actions to do on state changes
    void idleStateEntered();
    void recordingStateEntered();

private:
    // State Machinery variables and methods
    enum State {Idle, Ready, Recording};
    State currentState;
    QStateMachine *m_machine;
    void setupStateMachine();
    bool canRecord();

    // Recording helper classes, variables and methods
    Recorder* m_recorder;

    bool is_valid_recordingrect;
    bool checkDirectory(QDir desiredDir);
    bool checkRecordingRect(QRect desiredRect);
    QString nextFilename(QDir savingDir);
};


//Type of methods of RecordDockWidget that take no arguments and return a boolean, 
// to be used as a "checker" functions for conditional state transitions
typedef bool (RecordDockWidget::*condFun)();    

/***********************************************************************************************
 * Helper class
 */
class ConditionalTransition : public QSignalTransition
{
public:
    ConditionalTransition (RecordDockWidget *r, condFun f);
    
protected:
    bool eventTest(QEvent *event);

private:
    RecordDockWidget *widgetPtr;
    condFun testFunction;
};
/***********************************************************************************************
 * Helper class end
 */


} // namespace Lekture

#endif // RECORDDOCKWIDGET_H
