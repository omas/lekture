/***************************************************************************
 *   Copyright (C) 2012 by Orestes Mas <orestes@tsc.upc.edu>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

// own
#include "mainwindow.h"
#include "settings.h"
#include "lektureapp.h"
#include "recorder.h"

// qt
#include <QtDebug>
#include <QScrollBar>
#include <QLayout>

// KDE
#include <KConfigDialog>
#include <KStatusBar>
#include <KActionCollection>
#include <KFileDialog>
#include <KMessageBox>
#include <KIO/NetAccess>
#include <KSaveFile>
#include <QTextStream>
#include <KApplication>

namespace Lekture
{

MainWindow::MainWindow ( QWidget *parent )
    : KXmlGuiWindow ( parent )
{
    // Instantiate widgets:
    cdw = new ChalksDockWidget ( this );
    rdw = new RecordDockWidget ( this );
    // A board of desired size (perhaps larger than its visible area)
    m_board = new BlackBoard ( realBoardSize() );
    // Now instantiate a scrollable board viewer, of fixed size, and set board as its widget
    // Only vertical scroll bar is necessary, as board can be higher than its visible area,
    //    but not wider. Also, vertical scroll bar is set to always visible for convenience.
    m_boardView = new BoardView ( this, rdw->recorder()->getStdVideoSize() );

    // Set up previous widgets
    m_boardView->setBackgroundRole ( QPalette::Dark );
    m_boardView->setHorizontalScrollBarPolicy ( Qt::ScrollBarAlwaysOff );
    m_boardView->setVerticalScrollBarPolicy ( Qt::ScrollBarAlwaysOn );
    m_boardView->setSizePolicy ( QSizePolicy::Fixed,QSizePolicy::Fixed );
    m_boardView->setWidget ( m_board );
    // set up dock widgets
    setDockOptions ( KXmlGuiWindow::AllowNestedDocks|
                     KXmlGuiWindow::AllowTabbedDocks|
                     KXmlGuiWindow::AnimatedDocks );

    cdw->setFeatures ( QDockWidget::DockWidgetFloatable |
                       QDockWidget::DockWidgetMovable );
    cdw->setAllowedAreas ( Qt::LeftDockWidgetArea |
                           Qt::RightDockWidgetArea );
    // Inform BlackBoard of user's actions on ChalksDockWidget
    connect ( cdw, SIGNAL(chalkSelected()), m_board, SLOT(selectChalk()) );
    connect ( cdw, SIGNAL(eraserSelected()), m_board, SLOT(selectEraser()) );
    connect ( cdw, SIGNAL(newColorRequested(QColor)), m_board, SLOT(changeChalkColor(QColor)) );
//FIXME (if you can): I've discovered that qApp refers to MainWindow instead of QApplication!!!!
// so this doesn't work (even if you cast qApp to LektureApp)
// connect ( qApp, SIGNAL(tabletEraser()), this, SLOT(selectEraser()) );
// connect ( qApp, SIGNAL(tabletPen()), this, SLOT(selectChalk()) );
    // Make board state changes reflect into GUI
    connect ( m_board, SIGNAL(penColorChanged(QColor)),cdw,SLOT(changeCurrentColor(QColor)) );
    connect ( m_board, SIGNAL(chalkSelected()), cdw, SLOT(selectChalk()) );
    connect ( m_board, SIGNAL(eraserSelected()), cdw, SLOT(selectEraser()) );
    connect ( m_board, SIGNAL(boardSizeChanged()), this, SLOT(boardSizeChanged()) );
    addDockWidget ( Qt::LeftDockWidgetArea, cdw );

    rdw->setFeatures ( QDockWidget::DockWidgetFloatable |
                       QDockWidget::DockWidgetMovable );
    rdw->setAllowedAreas ( Qt::LeftDockWidgetArea |
                           Qt::RightDockWidgetArea );
    addDockWidget ( Qt::RightDockWidgetArea, rdw );

    // tell the KXmlGuiWindow that boardview is indeed the main widget
    setCentralWidget ( m_boardView );
    // accept dnd
    // FIXME: Perhaps only board / boardview may receive DnD events, as it makes sense,
    //        for instance, to drop an image or other kind of graphical object inside the board
    //        at some point during recording
    setAcceptDrops ( true );
    // then, setup our actions (first) and GUI
    setupActions();
    // a call to KXmlGuiWindow::setupGUI() populates the GUI
    // with actions, using KXMLGUI.
    // It also applies the saved mainwindow settings, if any, and ask the
    // mainwindow to automatically save settings if changed: window size,
    // toolbar position, icon size, etc.
    setupGUI();
    // add a status bar
    statusBar()->insertPermanentItem ( "Hola",1 );
    statusBar()->show();
    // accept dnd
    // FIXME: Perhaps only board / boardview may receive DnD events, as it makes sense,
    //        for instance, to drop an image or other kind of graphical object inside the board
    //        at some point during recording
    setAcceptDrops ( true );


    //TODO: Hey, this fixes size. So, I have to implement a good sizeHint()...
//     setFixedSize(sizeHint());
}

MainWindow::~MainWindow()
{
    delete m_boardView;
    delete cdw;
    delete rdw;
}

void MainWindow::enterIdleState()
{
    statusBar()->changeItem ( i18nc("State name", "Idle"),1 );
}

void MainWindow::enterRecordingState()
{
    statusBar()->changeItem ( i18nc ("State name", "Recording..."),1 );
    //TODO: It would be nice to lock window geometry during record, and unlock it when idle...
}

QRect MainWindow::getRecordingRect()
{
    return m_boardView->visibleBoardRect();
}

void MainWindow::boardSizeChanged()
{
//     m_boardView->updateBoardViewSize(visibleBoardSize());
//     adjustSize();
}

void MainWindow::clear()
{
    m_board->clear();
    m_boardView->verticalScrollBar()->triggerAction ( QAbstractSlider::SliderToMinimum );;
}

void MainWindow::loadSettings()
{
    m_board->updateBoardSize ( realBoardSize() );
    m_boardView->updateBoardViewSize ( rdw->recorder()->getStdVideoSize() );
}

void MainWindow::optionsPreferences()
{
    // Upon creation, we need to keep the preferences dialog alive to be able to access some internal widgets
    // from other parts of the code, so we DO NOT set the Qt::WA_DeleteOnClose attribute,
    // and we cate to avoid having 2 dialogs shown
    if ( CustomSettingsDialog::showDialog ( "settings" ) )  {
        return;
    }

    // If still not created, construct the main dialog object
    // We use a custom dialog derived from QWidget due to the presence of interrelated custom widgets
    settingsDialog = new CustomSettingsDialog ( this, "settings", Settings::self() );

    // Connect signals to slots
    connect ( settingsDialog, SIGNAL(settingsChanged(QString)), this, SLOT(loadSettings()) );
    connect ( settingsDialog, SIGNAL(settingsChanged(QString)), m_board, SLOT(loadSettings()) );
    connect ( settingsDialog, SIGNAL(settingsChanged(QString)), rdw, SLOT(loadSettings()) );
    // Connect Audio Source combobox with Recorder object
    settingsDialog->audioSrcComboPtr->setRecorder(rdw->recorder());
    // Show dialog
    settingsDialog->show();
}

void MainWindow::loadFile()
{
    m_board->loadFile ( KFileDialog::getOpenFileName ( KUrl(), "*.png",0,i18nc ( "Dialog title", "Load board contents" ) ) );
}

void MainWindow::saveFile()
{
    if ( !fileName.isEmpty() ) {
        saveFileAs ( fileName );
    } else {
        saveFileAs();
    }
}

void MainWindow::saveFileAs()
{
    saveFileAs ( KFileDialog::getSaveFileName ( KUrl(), "*.png" ) );
}

void MainWindow::saveFileAs ( const QString &outputFileName )
{
    m_board->saveFile ( outputFileName );
    fileName = outputFileName;
}

void MainWindow::setupActions()
{
    KStandardAction::quit ( qApp, SLOT(closeAllWindows()), actionCollection() );
    KStandardAction::clear ( this, SLOT(clear()), actionCollection() );
    KStandardAction::preferences ( this, SLOT(optionsPreferences()), actionCollection() );
    KStandardAction::open ( this, SLOT(loadFile()), actionCollection() );
    KStandardAction::save ( this, SLOT(saveFile()), actionCollection() );
    KStandardAction::saveAs ( this, SLOT(saveFileAs()), actionCollection() );
}

QSize MainWindow::realBoardSize()
{
    QSize realBoardSize = rdw->recorder()->getStdVideoSize();
    realBoardSize.rheight() *= Settings::boardVerticalSize();
    return ( realBoardSize );
}

} //namespace Lekture

#include "mainwindow.moc"
