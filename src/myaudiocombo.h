/*
    Custom class derived from QComboBox, knowing how to fill itself with
    apropiate values, and to save the selected value as String in KConfig
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MYAUDIOCOMBO_H
#define MYAUDIOCOMBO_H

//own
#include "recorder.h"

//Qt
#include <QComboBox>
#include <QEvent>
#include <QPointer>

//KDE
#include <KConfig>
#include <KConfigGroup>
#include <KComboBox>

namespace Lekture
{

//TODO: Save the selected value as string in KConfig when necessary

class MyAudioCombo : public QComboBox
{
    Q_OBJECT

public:
    explicit MyAudioCombo ( QWidget* parent = 0 );
    ~MyAudioCombo();
    bool isDefault();
    bool hasChanged();
    void setRecorder ( Recorder *newRecPtr = NULL );
    void setSoundSystemCombo ( KComboBox *newSndSysCombo = NULL );

signals:
    void otherThanDefaultSelected ( QString );

public slots:
    void refreshAudioDeviceList ( int );
    void applyChanges();
    void discardChanges();
    void setDefaults();

protected:
    bool event ( QEvent *event );

private:
    // Pointers to sibling objects
    QPointer<Recorder> recPtr;
    QPointer<KComboBox> sndSysCombo;
    // Config stuff
    KSharedConfigPtr config;
    KConfigGroup audioGroup;
    QString currentALSADevice;
    QString currentPulseDevice;
    QString lastALSADevice;
    QString lastPulseDevice;
    bool changed;    // Whether user changed the combo's value or not
    void readConfig();
    void writeConfig();
    
private slots:
    void itemChanged(QString);
};

}  // namespace Lekture

#endif // MYAUDIOCOMBO_H
