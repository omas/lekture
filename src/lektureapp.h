/*
    KApplication wrapper class to allow capturing of some tablet events
    We don't use it at this moment, but perhaps in the future.
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef LEKTUREAPP_H
#define LEKTUREAPP_H

// own
#include "blackboard.h"

// KDE
#include <KApplication>

namespace Lekture 
{

class LektureApp : public KApplication
{
    Q_OBJECT

public:
    explicit LektureApp ( bool GUIenabled = true );

signals:
    void tabletEraser();
    void tabletPen();
    
protected:
    virtual bool event ( QEvent* );
};

}  //namespace
#endif // LEKTUREAPP_H
