/*
    KApplication wrapper class to allow capturing of some tablet events
    We don't use it at this moment, but perhaps in the future.
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "lektureapp.h"

//Qt
#include <QTabletEvent>

//FIXME
#include <QDebug>

namespace Lekture
{

LektureApp::LektureApp ( bool GUIenabled ) : KApplication( GUIenabled )
{

}

bool LektureApp::event ( QEvent *event )
{
    // Code copied from Qt doc example
    if ( (event->type() == QEvent::TabletEnterProximity) ||
         (event->type() == QEvent::TabletLeaveProximity) ) {
        // If we want to react on this events, we need to cast them to QTabletEvent.
        // To do so, look at this example:
        //    static_cast<QTabletEvent *>(event)->pointerType().......;
        switch ( static_cast<QTabletEvent *>(event)->pointerType() ) {
            case QTabletEvent::Eraser:
                emit tabletEraser();
                break;
            default:
                emit tabletPen();
                break;
        }
        return true;
    }
   return QApplication::event( event );
}

}
