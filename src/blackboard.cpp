/***************************************************************************
 *   Copyright (C) 2012 by Orestes Mas <orestes@tsc.upc.edu>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

// own
#include "blackboard.h"
#include "settings.h"
#include "lektureapp.h"

// Qt
#include <QMouseEvent>
#include <QPainter>
#include <QPixmap>
#include <QBitmap>
#include <QMouseEventTransition>

namespace Lekture
{

BlackBoard::BlackBoard ( const QSize initialSize )
{
    // Set up pencil and eraser tools with proper parameters, ready to use
    setupDrawingTools();

    // Board setup
    boardSize = initialSize;
    setSizePolicy ( QSizePolicy::Fixed, QSizePolicy::Fixed );
    // Adjust minimum widget size to this size
    resize ( boardSize );
    // Create the underlying image where painting actually takes place
    brd = new QImage ( boardSize, QImage::Format_ARGB32_Premultiplied );
    clear();

    setupStateMachine();
}

BlackBoard::~BlackBoard()
{
    delete brd;
    delete chalk->stylus;
    delete chalk->cursor;
    delete chalk;
    delete eraser->stylus;
    delete eraser->cursor;
    delete eraser;
    // delete m_machine; // Not necessary, as m_machine is a child of "this"
}

void BlackBoard::clear()
{
    QPainter p ( brd );
    QRect r ( brd->rect() );
    p.fillRect ( r, eraser->stylus->color() );
    update();
    selectChalk();   // If erasing, return to drawing
}

void BlackBoard::changeChalkColor ( const QColor color )
{
    setChalkColor ( color );
    // Make State Machine switch
    //FIXME: mmmm... not sure it's good: maybe switch to ckalk only if color changed...
    selectChalk();
}

void BlackBoard::selectEraser()
{
    // Make State Machine switch
    emit toEraser();
}

void BlackBoard::selectChalk()
{
    // Make State Machine switch
    emit toChalk();
}

void BlackBoard::loadSettings()
{
    changeChalkColor ( Settings::defaultChalkColor() );
    setChalkWidth ( Settings::chalkWidth() );
    setBackgroundColor ( Settings::defaultBoardColor() );
    setEraserWidth ( Settings::eraserWidth() );
}

void BlackBoard::updateBoardSize ( const QSize newSize )
{
    if ( newSize != boardSize ) {
        delete brd;
        brd = new QImage ( newSize, QImage::Format_ARGB32_Premultiplied );
        boardSize = newSize;
        resize ( newSize );
        clear();
        emit boardSizeChanged();
    }
}

bool BlackBoard::loadFile ( const QString& inputFilename )
{
    return brd->load(inputFilename, "PNG");
}

bool BlackBoard::saveFile ( QString outputFileName )
{
    return brd->save(outputFileName, "PNG", 100);
}


void BlackBoard::mousePressEvent ( QMouseEvent* event )
{
    if ( event->button() == Qt::LeftButton ) {
        // Begin drawing: init drawing coords to current click point
        prevPos = curPos = event->pos();
        // change state
        is_painting = true;
        // Draw a point
        QPainter p ( brd );
        p.setRenderHint ( QPainter::Antialiasing, true );
        switch ( currentState ) {
        case Drawing:
            p.setPen ( *chalk->stylus );
            break;
        case Erasing:
            p.setPen ( *eraser->stylus );
            break;
        }
        p.drawPoint ( curPos );
        update();
    }
}

void BlackBoard::mouseMoveEvent ( QMouseEvent* event )
{
    if ( is_painting ) {
        curPos = event->pos();
        // Draw a line between previous and current points
        QPainter p ( brd );
        p.setRenderHint ( QPainter::Antialiasing, true );
        switch ( currentState ) {
        case Drawing:
            p.setPen ( *chalk->stylus );
            break;
        case Erasing:
            p.setPen ( *eraser->stylus );
            break;
        }
        p.drawLine ( prevPos,curPos );
        prevPos = curPos;
        update();
    }
}

void BlackBoard::mouseReleaseEvent ( QMouseEvent* event )
{
    Q_UNUSED ( event );
    is_painting = false;
}

void BlackBoard::paintEvent ( QPaintEvent* event )
{
    Q_UNUSED ( event );
    // Simply paint on screen the image stored in "brd"
    QPainter q ( this );
    q.drawImage ( QPoint ( 0,0 ),*brd );
}

void BlackBoard::tabletEvent ( QTabletEvent* event )
{
    switch (event->pointerType()) {
        case QTabletEvent::Eraser:
            if (currentState == Drawing and Settings::autoToolSelection()) selectEraser();
            break;
        case QTabletEvent::Pen:
            if (currentState == Erasing and Settings::autoToolSelection()) selectChalk();
            break;
        default:
            break;
    }
    switch (event->type()) {
        case QEvent::TabletPress:
            if ( !is_painting ) {
                is_painting = true;
                prevPos = curPos = event->pos();
                // Draw a point
                QPainter p ( brd );
                switch ( currentState ) {
                case Drawing:
                    p.setPen ( *chalk->stylus );
                    break;
                case Erasing:
                    p.setPen ( *eraser->stylus );
                    break;
                }
            }
            break;
        case QEvent::TabletRelease:
            if ( is_painting ) {
                is_painting = false;
                // Restore configured width
                chalk->stylus->setWidthF ( Settings::chalkWidth() );
                eraser->stylus->setWidthF ( Settings::eraserWidth() );
            }
            break;
        case QEvent::TabletMove:
            curPos = event->pos();
             if ( is_painting ) {
                QPainter p ( brd );
                tabletPaint ( p, event );
                prevPos = curPos;
            }
            break;
        default:
            break;
    }
    update();
}

void BlackBoard::tabletPaint ( QPainter& p, QTabletEvent* event )
{
    p.setRenderHint ( QPainter::Antialiasing, true );
    switch ( currentState ) {
        case Drawing:
            p.setPen ( *chalk->stylus );
            // Modulate stroke width according to pen pressure
            // Tablet pressure is a real between 0 and 1
            // We want the average pressure (0.5) lead to paint with nominal width
            chalk->stylus->setWidthF ( 2.0*event->pressure()*Settings::chalkWidth()*Settings::pressureFactor() );
            break;
        case Erasing:
            p.setPen ( *eraser->stylus );
            // We don't use "setEraserWidth" to set eraser width because this function also recalculates cursor
            // shape every time. Use this function only when changing eraser size from within config dialog.
            eraser->stylus->setWidthF ( 2.0*event->pressure()*Settings::eraserWidth()*Settings::pressureFactor() );
            break;
        }
    p.drawLine ( prevPos, curPos );
}


void BlackBoard::setChalkColor ( const QColor newColor )
{
    if ( newColor != chalk->stylus->color() ) {
        chalk->stylus->setColor ( newColor );
    }
}

void BlackBoard::setChalkWidth ( const double newWidth )
{
    if ( newWidth != chalk->stylus->widthF() ) {
        chalk->stylus->setWidthF ( newWidth );
    }
}

void BlackBoard::setEraserWidth ( const double newWidth )
{
    if ( newWidth != eraser->stylus->widthF() ) {
        delete eraser->cursor;
        delete eraser->stylus;
        delete eraser;
        eraser = new DrawingTool;
        eraser->stylus = new QPen ( Settings::defaultBoardColor() ); // Get color from configuration
        eraser->stylus->setStyle ( Qt::SolidLine );
        eraser->stylus->setCapStyle ( Qt::RoundCap );
        eraser->stylus->setWidthF ( newWidth );
        // Set up the erasing cursor, to have it ready for use
        // According to documentation, 32x32 cursors are the most portable
        // (this pixmap will be destroyed -I hope- on cursor destruction)
        QPixmap *eraserCursorPixmap = new QPixmap ( 32, 32 );
        // Red will be the mask color 
        eraserCursorPixmap->fill ( Qt::red );
        // Eraser cursor consists of 2 concentric circles black-white
        QPainter p;
        QPointF center = QPointF ( 16.0, 16.0 );
        p.begin ( eraserCursorPixmap );
        p.setPen ( Qt::white );
        p.drawEllipse ( center, newWidth/2, newWidth/2 );
        p.setPen ( Qt::black );
        p.drawEllipse ( center, newWidth/2 + 1.0, newWidth/2 + 1.0 );
        p.end();
        QBitmap mask = eraserCursorPixmap->createMaskFromColor ( Qt::red );
        eraserCursorPixmap->setMask ( mask );
        eraser->cursor = new QCursor ( *eraserCursorPixmap );
    }
}

void BlackBoard::setBackgroundColor ( const QColor newColor )
{
    if ( newColor != eraser->stylus->color() ) {
        eraser->stylus->setColor ( newColor );
        clear();
    }
}

void BlackBoard::setupDrawingTools()
{
    // Pen setup
    chalk = new DrawingTool;
    chalk->stylus = new QPen ( Settings::defaultChalkColor() );
    chalk->stylus->setStyle ( Qt::SolidLine );
    chalk->stylus->setCapStyle ( Qt::RoundCap );
    chalk->stylus->setJoinStyle ( Qt::RoundJoin );
    chalk->stylus->setWidthF ( Settings::chalkWidth() );
    chalk->cursor = new QCursor ( Qt::CrossCursor ); // Pen cursor is a standard one

    // Eraser part
    eraser = new DrawingTool;
    eraser->stylus = new QPen ( Settings::defaultBoardColor() ); // Get color from configuration
    eraser->stylus->setStyle ( Qt::SolidLine );
    eraser->stylus->setCapStyle ( Qt::RoundCap );
    eraser->stylus->setWidthF ( Settings::eraserWidth() );
    // Set up the erasing cursor, to have it ready for use
    // According to documentation, 32x32 cursors are the most portable
    // (this pixmap will be destroyed -I hope- on cursor destruction)
    QPixmap *eraserCursorPixmap = new QPixmap ( 32, 32 );
    // Red will be the mask color
    eraserCursorPixmap->fill ( Qt::red );
    // Eraser cursor consists of 2 concentric circles black-white
    QPainter p;
    QPointF center = QPointF ( 16.0, 16.0 );
    const double eraserWidth = Settings::eraserWidth();
    p.begin ( eraserCursorPixmap );
    p.setPen ( Qt::white );
    p.drawEllipse ( center, eraserWidth/2, eraserWidth/2 );
    p.setPen ( Qt::black );
    p.drawEllipse ( center, eraserWidth/2 + 1.0, eraserWidth/2 + 1.0 );
    p.end();
    QBitmap mask = eraserCursorPixmap->createMaskFromColor ( Qt::red );
    eraserCursorPixmap->setMask ( mask );
    eraser->cursor = new QCursor ( *eraserCursorPixmap );
    
    // variable initialisation
    prevPos = curPos = QPoint(0,0);
    is_painting = false;
}

void BlackBoard::setupStateMachine()
{
    // Machine and states
    m_machine = new QStateMachine ( this );
    QState *drawing = new QState();
    QState *erasing = new QState();
    drawing->addTransition ( this, SIGNAL(toEraser()), erasing );
    drawing->addTransition ( this, SIGNAL(toChalk()),drawing );
    erasing->addTransition ( this, SIGNAL(toChalk()), drawing );

    connect ( drawing, SIGNAL(entered()), this, SLOT(enterDrawingState()) );
    connect ( erasing, SIGNAL(entered()), this, SLOT(enterErasingState()) );

    m_machine->addState ( drawing );
    m_machine->addState ( erasing );
    m_machine->setInitialState ( drawing );
    m_machine->start();
}

void BlackBoard::enterDrawingState()
{
    currentState = Drawing;
    setCursor ( *chalk->cursor );
    // Notify other widgets that pen color has changed
    emit penColorChanged ( chalk->stylus->color() );
    emit chalkSelected();
}

void BlackBoard::enterErasingState()
{
    currentState = Erasing;
    setCursor ( *eraser->cursor );
    // Notify other widgets that pen color has changed
    emit penColorChanged ( eraser->stylus->color() );
    emit eraserSelected();
}

} // namespace Lekture
