/***************************************************************************
 *   Copyright (C) 2012 by Orestes Mas <orestes@tsc.upc.edu>               *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA .        *
 ***************************************************************************/

#include "mainwindow.h"

// Version number
#include "version_config.h"

//own
#include "lektureapp.h"

// KDE
#include <KDE/KAboutData>
#include <KDE/KCmdLineArgs>

static const char description[] =
    I18N_NOOP ( "An application to help creating video recordings of lectures" );

static const char version[] = LEKTURE_VERSION;

int main ( int argc, char **argv )
{
    KAboutData about ( "lekture", 0, ki18n ( "Lekture" ), version, ki18n ( description ),
                       KAboutData::License_GPL, ki18n ( "(C) 2012 Orestes Mas" ), KLocalizedString(), 0, "orestes@tsc.upc.edu" );
    about.addAuthor ( ki18n ( "Orestes Mas" ), KLocalizedString(), "orestes@tsc.upc.edu" );
    KCmdLineArgs::init ( argc, argv, &about );

    KCmdLineOptions options;
    options.add ( "+[URL]", ki18n ( "Document to open" ) );
    KCmdLineArgs::addCmdLineOptions ( options );
    Lekture::LektureApp app;

    Lekture::MainWindow *widget = new Lekture::MainWindow;

    // see if we are starting with session management
    if ( app.isSessionRestored() ) {
        RESTORE ( Lekture::MainWindow );
    } else {
        // no session.. just start up normally
        KCmdLineArgs *args = KCmdLineArgs::parsedArgs();
        if ( args->count() == 0 ) {
            //lekture *widget = new lekture;
            widget->show();
        } else {
            int i = 0;
            for ( ; i < args->count(); i++ ) {
                //lekture *widget = new lekture;
                widget->show();
            }
        }
        args->clear();
    }

    return app.exec();
}
