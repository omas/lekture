/*
    This file is part of "Lekture", a lecture recording software
    Copyright (C) 2012  Orestes Mas <orestes@tsc.upc.edu>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CHALKSDOCKWIDGET_H
#define CHALKSDOCKWIDGET_H

// own
#include <ui_chalks_dockwidget.h>

class KColorCells;

namespace Lekture
{

class ChalksDockWidget : public QDockWidget, public Ui::chalksDockWidget
{
    Q_OBJECT

public:
    explicit ChalksDockWidget ( QWidget* parent = 0, Qt::WindowFlags flags = 0 );
    virtual QSize sizeHint() const;

signals:
    void newColorRequested ( const QColor );
    void eraserSelected();
    void chalkSelected();

public slots:
    void changeCurrentColor ( const QColor );
    void selectChalk();
    void selectEraser();
    
private slots:
    void colorClicked ( int, const QColor );
    void toolToggled ( int );
};

} //namespace Lekture

#endif // CHALKSDOCKWIDGET_H
